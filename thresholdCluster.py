#This function runs the naive clustering algorithm written by Jason Jubinville
#
#Inputs: 
#Sampled Pandas Dataframe: dfn
#distance threshold: threshold
#Min Cluster Size: minCluster
#print debugg content: verbose
#similarity measure to be used
#
#Returns a Dictionary of Keyed by the Threshold Value
#
#Requires:
#Flott, Distance, Time, Pandas, External Utilities Library, Numpy
import time
import distance
import flott
import external_utils as eu
import numpy as np

#Run and Evaluate the Naive Algorithm
def runNaive(dfn, threshold, minCluster, measure, verbose):
    calculations=0
    clusterNumber = 1
    #Start of Cluster Loop
    #-------->
    t0=time.time()
    dfn['isClustered'] = False
    for iClusterA, rClusterA in dfn.iterrows():

        if(dfn.at[iClusterA, 'isClustered']==False):
            #set current Tweet to Clustered
            dfn.at[iClusterA, 'isClustered']=True
            dfn.at[iClusterA, 'clusterID']=clusterNumber

            #Iterate over all Tweets that are Unclustered
            for iClusterB, rClusterB in dfn[dfn.isClustered==False].iterrows():                                     

                #get the distance between the current Tweet and the Original Tweet
                if(measure=='TInfo'): distance = flott.nti_dist(rClusterA.Content, rClusterB.Content)
                elif(measure=='TCompl'): distance = flott.ntc_dist(rClusterA.Content, rClusterB.Content)
                elif(measure=='Jaccard'): distance = eu.invjsets(rClusterA.Content, rClusterB.Content)
                elif(measure=='LevChar'): distance = eu.LevChar(rClusterA.Content, rClusterB.Content)
                elif(measure=='LevToken'): distance = eu.LevToken(rClusterA.Content, rClusterB.Content)
                else: print('Error')
                calculations+=1
                
                if(threshold-distance>0):
                    dfn.at[iClusterB, 'isClustered']=True
                    dfn.at[iClusterB, 'clusterID']=clusterNumber
                    
            clusterNumber+=1
         
    timeElapsed = time.time()-t0
    print('Threshold: ',threshold,' Completed in:', round(timeElapsed,1), '    ', end='')
    return dfn, timeElapsed, calculations
    
   
    


## Run and Evaluate the ITWEC Version
def runITWEC(dfn, threshold, minCluster, measure, verbose):
    
    resultsDict = {}
    clusterNumber=1
    #Start of Run Loop
    #-------->
    t0=time.time()
    dfn['isClustered'] = False
    for iClusterA, rClusterA in dfn.iterrows():

        if(dfn.at[iClusterA, 'isClustered']==False):
            #Iterate over all Tweets that are Unclustered
            for iClusterB, rClusterB in dfn[dfn.isClustered==False].iterrows():                                     

                #get the distance between the current Tweet and the Original Tweet
                if(measure=='TInfo'): distance = flott.nti_dist(rClusterA.Content, rClusterB.Content) 
                elif(measure=='Jaccard'): distance = eu.invjsets(rClusterA.Content, rClusterB.Content)
                elif(measure=='LevChar'): distance = eu.LevChar(rClusterA.Content, rClusterB.Content)
                elif(measure=='LevToken'): distance = eu.LevToken(rClusterA.Content, rClusterB.Content)
                else: print('Error')
                calculations+=1
                
                if(threshold-distance>0):
                    dfn.at[iClusterB, 'clusterID']=clusterNumber
            
            #see if the cluster meets minimum requirements
            if(len(dfn[dfn.clusterID==c])>minCluster):
                dfn.loc[dfn.clusterID==c,'isClustered']=True
            else:
                dfn.loc[dfn.clusterID==c,'clusterID']=0
         
            #looptimer per cluster
            clusterNumber+=1
  
    
    timeElapsed = time.time()-t0
    print('Threshold: ',threshold,' Completed in:', round(timeElapsed,1), end='')
    return dfn, timeElapsed, calculations
    
    #End of Run Loop
    #----->
    
    #Evaluation Loop
    
    #Get Number of Clusters


def evaluateClusters(dfn, threshold, minCluster, measure, clusterType, verbose, elapsed, calcs):
    resultsDict={}
    n = len(dfn)
    
    resultsDict['timeElapsed']= elapsed
    resultsDict['calculations']= calcs 
    
    valueCounts = dfn.clusterID.value_counts()
    #get a list of clusters from the unique values
    clusterList=[i for i in list(valueCounts.index) if valueCounts[i] >= minCluster]   
    
    if(clusterType == 'Naive'):
        resultsDict['nClusters'] = len(clusterList)
        resultsDict['unClustered'] = n-sum(valueCounts[valueCounts>=minCluster])
        resultsDict['reduction']= (sum(valueCounts[valueCounts>=minCluster])+len(clusterList))/n
    
    if(clusterType == 'ITWEC'): 
        resultsDict['unClustered'] = len(dfn[dfn.clusterID==0])
        resultsDict['nClusters'] = len(clusterList)-1
        resultsDict['reduction']= (len(dfn[dfn.clusterID==0])+len(clusterList)-1)/n
    
    RMSDs = []
    clusterSizes =[]
    maxCluster = (0,0)
    t0=time.time()
    for cl in clusterList:
        i=0
        dist=[0]*len(dfn[dfn.clusterID==cl])**2
       
        for indexA, rowA in dfn[dfn.clusterID==cl].iterrows():
            for indexB, rowB in dfn[dfn.clusterID==cl].iterrows():
                
                
                if(measure=='TInfo'): dist[i] = flott.nti_dist(rowA.Content, rowB.Content) 
                elif(measure=='Jaccard'): dist[i] = eu.invjsets(rowA.Content, rowB.Content)
                elif(measure=='LevChar'): dist[i] = eu.LevChar(rowA.Content, rowB.Content)
                elif(measure=='LevToken'): dist[i] = eu.LevToken(rowA.Content, rowB.Content)
                else: print('error')
                
                i+=1
       
        if((len(dfn[dfn.clusterID==cl])>minCluster) and verbose):
            print('------------------>')
            print('Cluster:',cl)
            print('Elements:', len(dfn[dfn.clusterID==cl]))
            print('RMSD', np.sqrt(np.mean(np.square(dist))))
       
        RMSDs.append(np.sqrt(np.mean(np.square(dist))))
        
        
        if(cl!=0): clusterSizes.append(len(dfn[dfn.clusterID==cl])) 
        if(len(dfn[dfn.clusterID==cl]>maxCluster[1]) and cl != 0):
            maxCluster = (cl,len(dfn[dfn.clusterID==cl]))
   
    resultsDict['maxCluster'] = maxCluster
    resultsDict['clusterSizes'] = clusterSizes
    resultsDict['avgRMSD'] = np.mean(RMSDs)
    ##End of Evaluation Loop
    print('Evaluation Done:', round(time.time()-t0,1))
    return resultsDict

#get min max and other cluster values
def evaluateClusterMetrics(dfn, threshold, minCluster, measure, clusterType, verbose, elapsed, calcs):
    resultsDict={}
    n = len(dfn)
    
    resultsDict['timeElapsed']= elapsed
    resultsDict['calculations']= calcs 
    
    valueCounts = dfn.clusterID.value_counts()
    #get a list of clusters from the unique values
    clusterList=[i for i in list(valueCounts.index) if valueCounts[i] >= minCluster]   
    
    if(clusterType == 'Naive'):
        resultsDict['nClusters'] = len(clusterList)
        resultsDict['unClustered'] = n-sum(valueCounts[valueCounts>=minCluster])
        resultsDict['reduction']= (sum(valueCounts[valueCounts>=minCluster])+len(clusterList))/n
    
    if(clusterType == 'ITWEC'): 
        resultsDict['unClustered'] = len(dfn[dfn.clusterID==0])
        resultsDict['nClusters'] = len(clusterList)-1
        resultsDict['reduction']= (len(dfn[dfn.clusterID==0])+len(clusterList)-1)/n
    
    clusterDists={}
    clusterMetrics={}
    RMSDs = []
    clusterSizes =[]
    t0=time.time()
    yes=True
    for cl in clusterList:
        i=0
        dist=[]
        #evalutate the pair-wise intercluster distances
        clusterFrame = dfn[dfn.clusterID==cl].copy()
        for indexA, rowA in clusterFrame.iterrows():
            
            for indexB, rowB in clusterFrame.iterrows():
                
                if(indexA!=indexB):
                    if(measure=='TInfo'): dist.append(flott.nti_dist(rowA.Content, rowB.Content))
                    elif(measure=='Jaccard'): dist.append(eu.invjsets(rowA.Content, rowB.Content))
                    elif(measure=='LevChar'): dist.append(eu.LevChar(rowA.Content, rowB.Content))
                    elif(measure=='LevToken'): dist.append(eu.LevToken(rowA.Content, rowB.Content))
                    else: print('error')
                    
                    i+=1
                    
    
        RMSDs.append(np.sqrt(np.mean(np.square(dist))))
        
        #cl only == 0 in ITWEC clustering for unclustered data
        if(cl!=0): 
            clusterSizes.append(len(dfn[dfn.clusterID==cl]))
            
            clusterSize = len(dfn[dfn.clusterID==cl])
            clusterMax = np.max(dist)
            clusterMin = np.min(dist)
            clusterSTD = np.std(dist)
            clusterMean = np.mean(dist)
            clusterMedian = np.median(dist)
            tweetGroup, clusterComplexity = eu.flottComplexity(dfn, cl)
            clusterDists[cl]=dist
            clusterMetrics[cl] = [clusterSize, clusterMax, clusterMin,
                                  clusterSTD, clusterMean, clusterMedian,
                                  clusterComplexity]
            
    resultsDict['clusterSizes'] = clusterSizes
    resultsDict['avgRMSD'] = np.mean(RMSDs)
    ##End of Evaluation Loop
    print('Evaluation Done:', round(time.time()-t0,1))
    #print(dfn)
    return dfn, resultsDict, clusterMetrics, clusterDists