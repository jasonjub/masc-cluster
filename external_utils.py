#these functions support the Pandas Dataframe cleaning and operations
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
import subprocess
import distance


#plots a bag of words using pandas series and mysterious plotting algorithms
def pltBW(arrayBoW, terms, xlabel, ylabel, title):
   
    s = pd.Series(arrayBoW).str.lower()
    y = s.value_counts()
    
    y[0:terms].plot(kind='bar')
    plt.title(title, fontsize=20)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.show()
    
    return y

def pltUserFreq(df, nPosts, xlabel, ylabel, title):
   
    y = df.UID.value_counts()
    y = y[y>nPosts]
    y.sort_index()    
    y.sort_index().plot(kind='bar')
    plt.title(title, fontsize=20)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.show()


#plot the number of terms in a histogram of the number of terms in DF
def pltNTerms(df, xlabel, ylabel, title):
    
    y = df.nTerms.value_counts()
    y.sort_index()    
    y.sort_index().plot(kind='bar')
    plt.title(title, fontsize=20)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.show()
   

#intercluster distances
def interJaccardDist(df, samplePercentage):
    dist = []
    i=0
    dfn = df.sample(len(df)*samplePercentage)
    for indexA, rowA in dfn.iterrows():
        for indexB, rowB in dfn.iterrows():
            dist[i] = invjsets(rowA.Content, rowB.Content)
        i+=1
    return np.sqrt(np.mean(np.square(dist)))

def interTInfoDist(df, samplePercentage):
    dist = []
    i=0
    dfn = df.sample(len(df)*samplePercentage)
    for indexA, rowA in dfn.iterrows():
        for indexB, rowB in dfn.iterrows():
            dist[i] = runFlott(rowA.Content, rowB.content.Content)
        i+=1
    return np.sqrt(np.mean(np.square(dist)))


#get the hashtags from a dataframe
def getHashtags(df, dset):
    array = []
    countError = 0
    for index, series in df.iterrows():
        try:
            if(dset=='Content'):tags = series.Content
            elif(dset=='Trimmed'):tags = series.Trimmed
            array.extend([tag.strip('\'"\u2026"#,!\./-') for tag in tags.split() if tag.startswith("#")])
        except AttributeError:
            #print every hundredth error -- this will break at scale should do % based
            #if(countError%131==0): print(series.Content)
            countError+=1
    return array, countError


#create a row of Trimmed strings that do not contain the major result
## DEPRECATED IN FAVOUR OF A LAMBDA FUNCTION
def createTrimmed(df, hashResult):
    countError = 0
    for index, series in df.iterrows():
        try:
            tags = series.Content.lower()
            #strip the hashtag from the search query
            df.set_value(index,'Trimmed', ''.join(tags.split(hashResult)))
        except AttributeError:
            countError+=1
    return df, countError


#get and single string of all Tweets joined
def grpTweets(df, dset):
    grpTweets = ''
    countError = 0
    for index, series in df.iterrows():
        try:
            if(dset=='Content'):tags = series.Content
            elif(dset=='Trimmed'):tags = series.Trimmed
            grpTweets = ''.join([grpTweets, tags])
        except AttributeError:
            countError+=1
        #except TypeError:
        #    if(math.isnan(tags)): grpTweets = join([grpTweets, ''])
    return grpTweets.strip(), countError


#returns the Jaccard similarity between to tokenized sets.
#This function expects two strings input and outputs a float
def invjsets(a, b):
    s1 = set(a.split())
    s2 = set(b.split())
    jss = float(len(s1.intersection(s2)))/float(len(s1.union(s2)))
    #print(len(set_c), len(set_a), len(set_b))
    return 1-jss


#levenshtein distances for Characters and Tokens
def LevChar(a,b):
    maxLength = max(len(a), len(b))
    dist = distance.levenshtein(a,b)/maxLength
    return dist


def LevToken(a,b):
    aTokens = a.split()
    bTokens = b.split()
    maxLength = max(len(aTokens), len(bTokens))
    dist = distance.levenshtein(aTokens,bTokens)/maxLength
    return dist



#assumes correct hardcoded directories, and two string inputs
def runFlott(switchString, stringA, stringB):
    a = subprocess.run(["/code/libflott-master/flott", switchString, "-q",
                    "-S", stringA,
                    "-S", stringB],
                    stdout=subprocess.PIPE)
    return float(a.stdout.split()[0].decode('utf-8'))

def plotResultsDict(results, source, n):
    return
    

def flottComplexity(dfn, clusterID):
    switchString = '-v0'
    
    stringGroup = ''.join(list(dfn.Content[dfn.clusterID==clusterID]))
    
    a = subprocess.run(["/code/libflott-master/flott", switchString, "-c",
                    "-S", stringGroup],
                    stdout=subprocess.PIPE)
    
    flottComplexity = float(a.stdout.split()[0].decode('utf-8'))
    
    return stringGroup, flottComplexity

def getUUID(searchName):
    searchName = searchName.lower()
    
    if(searchName=='london'): uuid='ea5eacee-d384-4bc2-a40d-29c8139d12c9'
    elif(searchName=='royalwedding'):uuid='c41ea6bf-0e24-4e02-bd1d-71f7d1ba018a'
    elif(searchName=='vancouver'):uuid='5baa40f6-95b2-4c2d-a01b-c60c766a6711'
    elif(searchName=='worldcup'):uuid='624286b3-8895-476e-9cb1-6576e1dabe46'
    else: raise ValueError('Name was not found in the list of UUIDs') 
    
    return uuid